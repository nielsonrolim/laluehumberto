class HomeController < ApplicationController
  def index
  end

  def rsvp
    @rsvp_form = RsvpForm.new
  end

  def send_rsvp
    all_error_required = params[:all_error_required]
    all_input_id = params[:all_input_id]

    if not all_error_required.nil?
      output = {type: 'text', text: params[:all_error_required]}
    elsif not all_input_id.nil?


      @rsvp_form = RsvpForm.new
      @rsvp_form.name = params[:inputname]
      @rsvp_form.email = params[:inputemail]
      @rsvp_form.attending = params['events-checkbox_label'] + params['events-checkbox']
      @rsvp_form.message = params[:inputmessage]

      if @rsvp_form.deliver
        output = {type: 'success', text: 'Confirmação enviada. Agradecemos muito!'}
      else
        output = {type: 'error', text: 'Houve um problema ao enviar sua confirmação. Desculpe-nos.'}
      end

    end

    respond_to do |format|
      format.json { render json: output }
    end
  end

  def ceremony
  end
end
