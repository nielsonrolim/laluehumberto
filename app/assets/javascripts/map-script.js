	
	'use strict';	
	
	// CHECK WINDOW RESIZE
	var is_windowresize = false;
	$(window).resize(function(){
		is_windowresize = true;
	});
	
	
	//INITIALIZE MAP
	function initialize() {
		
		//DEFINE MAP OPTIONS
		//=======================================================================================
  		var mapOptionsCeremony = {
    		zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP,	
    		center: new google.maps.LatLng(-7.1164882,-34.8846569),
			panControl: true,
  			zoomControl: true,
  			mapTypeControl: true,
  			//scaleControl: false,
  			streetViewControl: true,
  			overviewMapControl: true,
			//rotateControl:true,
            scrollwheel: false

  		};

        var mapOptionsParty = {
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: new google.maps.LatLng(-7.0868595,-34.833902),
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            //scaleControl: false,
            streetViewControl: true,
            overviewMapControl: true,
            //rotateControl:true,
            scrollwheel: false

        };

		//CREATE NEW MAP
		//=======================================================================================
  		var mapCeremony = new google.maps.Map(document.getElementById('map-ceremony'), mapOptionsCeremony);

        var mapParty = new google.maps.Map(document.getElementById('map-party'), mapOptionsParty);


        //MARKER ICON
		//=======================================================================================
		//var image = 'facebook30.svg';
		
		//ADD NEW MARKER
		//=======================================================================================
  		/*var marker = new google.maps.Marker({
    		position: map.getCenter(),
   		 	map: map,
    		title: 'Click to zoom',
			icon: image
  		});
		
		var marker1 = new google.maps.Marker({
    		position: new google.maps.LatLng(-12.042559, -77.027426),
   		 	map: map,
    		title: 'Click to zoom'
  		});*/
		
		
		//ADD NEW MARKER WITH LABEL
		//=======================================================================================
		var markerCeremony = new MarkerWithLabel({
       		position: new google.maps.LatLng(-7.1164882,-34.8846569),
       		draggable: false,
       		raiseOnDrag: false,
       		icon: ' ',
       		map: mapCeremony,
         	labelContent: '<div class="de-icon circle medium-size" style="background-color:#FFF; border:2px solid #f0394d"><i class="de-icon-plus-outline" style="color:#f0394d"></i></div>',
       		labelAnchor: new google.maps.Point(29, 20),
       		labelClass: "labels" // the CSS class for the label
     		});

        var markerParty = new MarkerWithLabel({
            position: new google.maps.LatLng(-7.0868595,-34.833902),
            draggable: false,
            raiseOnDrag: false,
            icon: ' ',
            map: mapParty,
            labelContent: '<div class="de-icon circle medium-size" style="background-color:#FFF; border:2px solid midnightblue"><i class="de-icon-music-3" style="color:midnightblue"></i></div>',
            labelAnchor: new google.maps.Point(29, 20),
            labelClass: "labels" // the CSS class for the label
        });

		
		//INFO WINDOWS
		//=======================================================================================
		var contentStringCeremony = '<div>'+
		'CERIMÔNIA RELIGIOSA';
      	'</div>';
		
		var contentStringParty = '<div>'+
		'RECEPÇÃO';
      	'</div>';

 	 	var infowindowCeremony = new google.maps.InfoWindow({
      		content: contentStringCeremony
  		});
		
		var infowindowParty = new google.maps.InfoWindow({
      		content: contentStringParty
  		});
		
		
		
		//OPEN INFO WINDOWS ON LOAD
		//=======================================================================================
        infowindowCeremony.open(mapCeremony,markerCeremony);

        infowindowParty.open(mapParty,markerParty);
		

		//ON MARKER CLICK EVENTS
		//=======================================================================================
  		/*google.maps.event.addListener(marker, 'click', function() {
   	 		map.setZoom(17);
    		map.setCenter(marker.getPosition());
			infowindow.open(map,marker);
  		});*/
		
		google.maps.event.addListener(markerCeremony, 'click', function() {
            infowindowCeremony.open(mapCeremony,markerCeremony);
            mapCeremony.setZoom(17);
            mapCeremony.setCenter(markerCeremony.getPosition());
  		});

        google.maps.event.addListener(markerParty, 'click', function() {
            infowindowParty.open(mapParty,markerParty);
            mapParty.setZoom(17);
            mapParty.setCenter(markerParty.getPosition());
        });
		
		//ON BOUND EVENTS AND WINDOW RESIZE
		//=======================================================================================
		google.maps.event.addListener(mapCeremony, 'bounds_changed', function() {
			if (is_windowresize)
			{
				//map.setCenter(marker.getPosition());
				window.setTimeout(function() {
                    mapCeremony.panTo(markerCeremony.getPosition());
    			}, 500);
			}
			is_windowresize=false;
		});

        google.maps.event.addListener(mapParty, 'bounds_changed', function() {
            if (is_windowresize)
            {
                //map.setCenter(marker.getPosition());
                window.setTimeout(function() {
                    mapParty.panTo(markerParty.getPosition());
                }, 500);
            }
            is_windowresize=false;
        });
	}

	// LOAD GMAP
	google.maps.event.addDomListener(window, 'load', initialize);
	
	
