// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery-ujs
//= require jquery-ui

//= require bootstrap
//= require pace
//= require modernizr/modernizr
//= require devicejs
//= require tinynav/tinynav.min
//= require smoothscroll/jquery.smooth-scroll.min
//= require flexslider
//= require sticky
//= require waypoint/jquery.waypoints.min
//= require jquery-ui-widget/jquery.ui.widget
//= require jquery-doubletaptogo/jquery.dcd.doubletaptogo
//= require vide
//= require stellar
//= require masonry/masonry.pkgd.min
//= require countdown/jquery.plugin
//= require countdown/jquery.countdown
//= require countdown/jquery.countdown-custom-label
//= require magnific-popup
//= require owlcarousel/owl.carousel
//= require map-script
//= require markerlabel/markerwithlabel

//= require script
//= require main-slider-fade
//= require rsvp/rsvp

//= require_tree .
