class RsvpForm < MailForm::Base
  attribute :name, :validate => true
  attribute :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :attending
  attribute :message
  #attribute :file, :attachment => true

  # attribute :message, :validate => true
  #attribute :nickname,  :captcha  => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    subject = 'RSVP de ' + self.name
    {
        :subject => %("#{subject}"),
        :to => "nielson@bitmine.com.br, lnplins@gmail.com",
        :from => %("[RSVP] #{name}" <#{email}>)
    }
  end
end
