require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get home_index_url
    assert_response :success
  end

  test "should get rsvp" do
    get home_rsvp_url
    assert_response :success
  end

  test "should get ceremony" do
    get home_ceremony_url
    assert_response :success
  end

end
