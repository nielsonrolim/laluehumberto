require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Laluehumberto
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom settings
    config.time_zone = 'America/Recife'
    # config.i18n.enforce_available_locales = false
    # config.i18n.default_locale = 'pt-BR'

    config.action_mailer.default_url_options = { host: ENV['SMTP_DOMAIN'], port: 80 }
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
        address: ENV['SMTP_ADDRESS'],
        port: ENV['SMTP_PORT'],
        domain: ENV['SMTP_DOMAIN'],
        user_name: ENV['SMTP_USERNAME'],
        password: ENV['SMTP_PASSWORD'],
        authentication: ENV['SMTP_AUTHENTICATION'],
        enable_starttls_auto: ENV['SMTP_ENABLE_STARTTLS_AUTO']
    }

  end
end
