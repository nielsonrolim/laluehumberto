Rails.application.routes.draw do
  get 'home/ceremony'

  get 'home/rsvp'

  root 'home#index'

  get 'home/index'

  get 'rsvp' => 'home#rsvp', as: :rsvp
  get 'ceremony' => 'home#ceremony', as: :ceremony

  post 'send_rsvp', to: 'home#send_rsvp', as: :rsvp_forms

  namespace :admin do
    root 'home#index'
    get 'home/index'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
